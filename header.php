<?php 

	$agency1_name = get_field('agency1_name', 'option');
	$agency1_phone = get_field('agency1_phone', 'option');
	
	$agency2_name = get_field('agency2_name', 'option');
	$agency2_phone = get_field('agency2_phone', 'option');
	
	$custom_css = get_field('custom_css', 'option');
	
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
    	<title><?php bloginfo('name'); ?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" type="image/png" href="<?php echo THEME_URL; ?>/favicon.png"/>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Dosis:400,200,300,500,600,700,800&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" media="all" href="<?php echo THEME_URL; ?>/assets/css/loader.css">
		<link rel="stylesheet" media="all" href="<?php echo THEME_URL; ?>/assets/scripts/lib/Magnific-Popup-master/dist/magnific-popup.css">
		<link rel="stylesheet" media="all" href="<?php echo THEME_URL; ?>/assets/style.css">
        <?php wp_head(); ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-85968375-1', 'auto');
  ga('send', 'pageview');

</script>
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-22988185-3', 'auto');
			ga('send', 'pageview');
		</script>
<script type="text/javascript">
 var goadservicesq = goadservicesq || [];
 try {
   goadservicesq.push(
     [
       "_ENTRY"
     ]
   );
   (function() {
     var goadservices = document.createElement('script');
     goadservices.type = 'text/javascript';
     goadservices.async = true;
     goadservices.src = '//t.goadservices.com/engine/5994cff8-889f-4857-9bfa-edfbec410400';

     var id_s = document.cookie.indexOf('__goadservices=');
     if (id_s != -1) {
       id_s += 15;
       var id_e = document.cookie.indexOf(';', id_s);
       if (id_e == -1) {
         id_e = document.cookie.length;
       }
       goadservices.src += '?id='+document.cookie.substring(id_s, id_e);
     }

     var s = document.getElementsByTagName('script')[0];
     s.parentNode.insertBefore(goadservices, s);
   })();
 } catch (e) {}
	</script>
	<script type="text/javascript">
	(function($) {
	    $(window).load(function() {
		$('#status').delay(350).fadeOut();
		$('#preloader').delay(500).fadeOut('slow');
		$('body').delay(500).css({'overflow':'visible'});
	    });
	})(jQuery)
	</script>
	<noscript><style>#preloader{display: none !important;}</style></noscript>

</head>
	<body class="<?php if(basename(get_page_template()) == 'front-page.php') : ?>homepage<?php endif; ?>">
		<!-- Google Tag Manager -->
		<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KC4VCR"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-KC4VCR');</script>
		<!-- End Google Tag Manager -->
		<div class="js-facebook">
			<div class="fb-page" data-href="https://www.facebook.com/klinikavesuna" data-width="290" data-height="390" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/klinikavesuna"><a href="https://www.facebook.com/klinikavesuna">Klinika Vesuna</a></blockquote></div></div>
		</div> <!-- .js-facebook -->
		<div id="preloader">
			<div id="status"><div id="loaderImage"></div></div>
		</div>
		
		<div id="top">
			<div class="row">
				<div class="container">
					<div class="top-left">
						<span class="text-uppercase"><?php echo _e('Nasze Kliniki:','vesuna'); ?> </span>
						<span class="f-white"><?php echo $agency1_name; ?> </span>
						<span class="f-orange">- <?php echo _e('Tel.','vesuna'); ?> <?php echo $agency1_phone; ?></span>
						<span class="f-white"><?php echo $agency2_name; ?> </span>
						<span class="f-orange">- <?php echo _e('Tel.','vesuna'); ?> <?php echo $agency2_phone; ?></span>
					</div>
					<div class="top-right">
						<form action="<?php if(ICL_LANGUAGE_CODE!='en') : ?>/<?php else : ?>/en/<?php endif; ?>" method="get" class="header-search">
                            <input type="hidden" name="search" value="website">
							<input type="search" name="s" value="" class="search" placeholder="<?php _e('Szukaj','vesuna'); ?>">
                            <input type="submit" value="">
						</form>           
						<?php do_action('icl_language_selector'); ?>
						<?php if(ICL_LANGUAGE_CODE!='en') : ?><a href="/blog" class="text-uppercase" style="position: relative; z-index: 1000000;">Blog</a><?php endif; ?>
					</div>
				</div>
			</div>
		</div>
		
		<nav class="navbar navbar-default" role="navigation" id="menu-header">
			<div class="container">
				<div class="navbar-header">
					<a href="/" title=""><img src="http://vesuna.pl/wp-content/themes/Vesuna/assets/images/logo_slogan.png" alt="Klinika medycyny estetycznej Vesuna" class="logo" width="686" height="261"></a>
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-main"> 

					
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-main"> 
						<span class="sr-only">Toggle navigation</span> 
						<span class="icon-bar"></span> <span class="icon-bar"></span> 
						<span class="icon-bar"></span> 
					</button>
				</div>
				<div class="collapse navbar-collapse" id="navbar-collapse-main">
					<ul class="nav navbar-nav navbar-left nav-left desktop-menu">
						<?php 
							wp_nav_menu(array(
							'container' => '',
							'theme_location' => 'top1',
							'walker' => new page_NavigationWalker,
							'menu_class'      => "",
							'items_wrap'      => '%3$s',
							));
					  ?>
					</ul>
					<nav class="mobile-menu">
						<?php
							wp_nav_menu(array(
								'container' => '',
								'theme_location' => 'top1',
								'walker' => new Walker_Nav_Menu_Dropdown(),
								'items_wrap' => '<select onchange="if(this.value) window.location.href = this.value;">%3$s</select>',
							)); 
						?>
					</nav>
					<ul class="nav navbar-nav navbar-left nav-center desktop-menu">
						<?php 
							 wp_nav_menu(array(
							 'container' => '',
							 'theme_location' => 'top2',
							 'walker' => new page_NavigationWalker,
							 'menu_class'      => "",
							 'items_wrap'      => '%3$s',
							 ));
						?>
					</ul>
					<nav class="mobile-menu">
						<?php
							wp_nav_menu(array(
								'container' => '',
								'theme_location' => 'top2',
								'walker' => new Walker_Nav_Menu_Dropdown(),
								'items_wrap' => '<select onchange="if(this.value) window.location.href = this.value;">%3$s</select>',
							)); 
						?>
					</nav>
					<ul class="nav navbar-nav navbar-left nav-right desktop-menu">
						<?php 
							 wp_nav_menu(array(
							 'container' => '',
							 'theme_location' => 'top3',
							 'walker' => new page_NavigationWalker,
							 'menu_class'      => "",
							 'items_wrap'      => '%3$s',
							 ));
						?>
					</ul>
					<nav class="mobile-menu">
						<?php
							wp_nav_menu(array(
								'container' => '',
								'theme_location' => 'top3',
								'walker' => new Walker_Nav_Menu_Dropdown(),
								'items_wrap' => '<select onchange="if(this.value) window.location.href = this.value;">%3$s</select>',
							)); 
						?>
					</nav>
					<div id="fb-root"></div>
					<script>(function(d, s, id) {
					  var js, fjs = d.getElementsByTagName(s)[0];
					  if (d.getElementById(id)) return;
					  js = d.createElement(s); js.id = id;
					  js.src = "//connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v2.4";
					  fjs.parentNode.insertBefore(js, fjs);
					}(document, 'script', 'facebook-jssdk'));</script>
					<ul class="navbar-h">
					<?php 
						$defaults = array(
							'menu'            => 'second',
							'container'       => 'ul',
							'menu_class'      => 'nav',
							'items_wrap'      => '%3$s',
						);
						wp_nav_menu($defaults); 
					?>
					<li class="fcb_like_button">
					<div class="fb-like" data-href="https://www.facebook.com/klinikavesuna" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div>
					</li>
					</ul>
				</div>
			</div>
		</nav><span></span>

